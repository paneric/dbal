<?php

declare(strict_types=1);

namespace Paneric\DBAL;

class QueryBuilder
{
    private SequencePreparator $sequencePreparator;
    private string $query;
    private ?string $selectQuery;


    public function __construct(SequencePreparator $sequencePreparator)
    {
        $this->sequencePreparator = $sequencePreparator;
    }


    public function getQuery(): string
    {
        return $this->query;
    }

    public function setSelectQuery(?string $selectQuery): void
    {
        $this->selectQuery = $selectQuery;
    }
    
    
    public function select(string $table, array $fields = ['*']): self
    {
        if ($this->selectQuery) {
            $this->query = $this->selectQuery;
            
            return $this;
        }
        
        $selectSequence = 'SELECT ';

        foreach ($fields as $field) {

            $selectSequence .= $field.', ';
        }

        $this->query = sprintf(
            '%s %s %s',
            rtrim($selectSequence, ', '),
            'FROM',
            $table
        );

        return $this;
    }

    public function init(string $query): self
    {
        $this->query = $query;

        return $this;
    }

    public function where(array $criteria, string $operator = 'AND'): self
    {
        if (empty($criteria)) {
            return $this;
        }

        $whereSequence = 'WHERE (';

        foreach ($criteria as $key => $value) {

            $whereSequence .= sprintf('%s%s%s %s ', $key, '=:', $key, $operator);
        }

        $this->query = sprintf(
            '%s %s)',
            $this->query,
            rtrim($whereSequence, ' '.$operator.' ')
        );

        return $this;
    }

    public function whereIn(array $values, string $field, string $not = null, string $operator = null): self
    {
        $keys = array_keys($values);

        $whereInSequence = '';

        foreach ($keys as $key) {
            $whereInSequence .= ':' . $key . ', ';
        }

        $this->query = sprintf(
            '%s %s %s%s IN (%s)',
            $this->query,
            ($operator === null ? 'WHERE' : strtoupper($operator)),
            $field,
            ($not === null || $not === '' ? '' : ' ' . strtoupper($not)),
            rtrim($whereInSequence, ', ')
        );

        return $this;
    }

    public function whereSame(array $criteriaSame, string $operator = 'OR'): self
    {
        if (empty($criteriaSame)) {
            return $this;
        }

        $whereSequence = 'WHERE ';

        foreach ($criteriaSame as $keySame => $criteria) {

            if (empty($criteria)) {
                return $this;
            }

            foreach ($criteria as $key => $value) {
                $whereSequence .= sprintf('%s%s%s %s ', $keySame, '=:', $key, $operator);
            }
        }

        $this->query = sprintf(
            '%s %s',
            $this->query,
            rtrim($whereSequence, ' '.$operator.' ')
        );

        return $this;
    }

    public function whereLike(array $criteriaLike, array $operators = ['OR']): self
    {
        if (empty($criteriaLike)) {
            return $this;
        }

        $whereSequence = 'WHERE ';

        $operator = '';

        $operatorIndex = -1;
        $operatorsNumber = count($operators);

        foreach ($criteriaLike as $keyLike => $criteria) {

            if (empty($criteria)) {
                return $this;
            }

            $operatorIndex++;
            $operator = $operators[$operatorIndex];

            $likeSequence = '';
            foreach ($criteria as $key => $value) {
                $likeSequence .= sprintf('%s %s%s %s ', $keyLike, 'LIKE :', $key, $operator);
            }

            $likeSequence = '(' . rtrim($likeSequence, ' '.$operator.' ') . ')';

            $whereSequence .= $likeSequence;

            if ($operatorIndex > -1 && $operatorIndex < $operatorsNumber - 1) {
                $operatorIndex++;
                $operator = $operators[$operatorIndex];

                $whereSequence .= ' ' . $operator . ' ';
            }
        }

        $this->query = sprintf(
            '%s %s',
            $this->query,
            rtrim($whereSequence, ' '.$operator.' ')
        );

        return $this;
    }

    public function orderBy(array $orderBy): self
    {
        $orderBySequence = 'ORDER BY ';

        foreach ($orderBy as $field => $order) {

            $orderBySequence .= $field.' '.$order.', ';
        }

        $this->query = sprintf(
            '%s %s',
            $this->query,
            rtrim($orderBySequence, ', ')
        );

        return $this;
    }

    public function limit(): self
    {
        $limitOffsetSequence = sprintf('%s %s', 'LIMIT', ':limit');

        if ($limitOffsetSequence !== '') {
            $this->query = sprintf(
                '%s %s',
                $this->query,
                $limitOffsetSequence
            );
        }

        return $this;
    }

    public function offset(): self
    {
        $limitOffsetSequence = sprintf('%s %s', 'OFFSET', ':offset');

        if ($limitOffsetSequence !== '') {
            $this->query = sprintf(
                '%s %s',
                $this->query,
                $limitOffsetSequence
            );
        }

        return $this;
    }


    public function insert(string $table, array $fields): self
    {
        $insertSequences = $this->sequencePreparator->prepareInsertSequences($fields);

        $this->query = sprintf(
            '%s %s %s %s %s',
            'INSERT INTO',
            $table,
            $insertSequences['names'],
            'VALUES',
            $insertSequences['values']
        );

        return $this;
    }

    public function insertMultiple(string $table, array $fields): self
    {
        $insertSequences = $this->sequencePreparator->prepareInsertMultipleSequences($fields);

        $this->query = sprintf(
            '%s %s %s %s %s',
            'INSERT INTO',
            $table,
            $insertSequences['names'],
            'VALUES',
            $insertSequences['values']
        );

        return $this;
    }

    public function update(string $table, array $fields): self
    {
        $this->query = sprintf(
            '%s %s',
            sprintf('%s %s %s', 'UPDATE', $table, 'SET'),
            $this->sequencePreparator->prepareUpdateSequence($fields)
        );

        return $this;
    }

    public function updateMultiple(string $table, array $fieldsSets, string $idKey): self
    {
        $insertSequences = $this->sequencePreparator->prepareInsertMultipleSequences($fieldsSets);
        $onDuplicateSequence = $this->sequencePreparator->prepareOnDuplicateSequences($fieldsSets[0], $idKey);

        $this->query = sprintf(
            '%s %s %s %s %s %s',
            'INSERT INTO',
            $table,
            $insertSequences['names'],
            'VALUES',
            $insertSequences['values'],
            $onDuplicateSequence
        );

        return $this;
    }

    public function updateSame(string $table, array $fields, array $criteriaInSet, string $field): self
    {
        $this->query = sprintf(
            '%s %s',
            sprintf('%s %s %s', 'UPDATE', $table, 'SET'),
            $this->sequencePreparator->prepareUpdateSequence($fields),
        );

        $this->whereIn($criteriaInSet, $field);

        return $this;
    }

    public function delete(string $table, array $criteria): self
    {
        $this->query = sprintf('%s %s', 'DELETE FROM', $table);

        $this->where($criteria);

        return $this;
    }

    public function deleteMultiple(string $table, array $criteriaInSet, string $field): self
    {
        $this->query = sprintf(
            '%s %s',
            'DELETE FROM',
            $table
        );

        $this->whereIn($criteriaInSet, $field);

        return $this;
    }
}
