<?php

declare(strict_types=1);

namespace Paneric\DBAL;

interface TransactionInterface
{
    public function adaptManager(string $table, string $daoClass, int $fetchMode): void;
}
