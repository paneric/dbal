<?php

declare(strict_types=1);

namespace Paneric\DBAL;

use Exception;
use InvalidArgumentException;
use PDO;
use PDOStatement;
use RuntimeException;

class PDOAdapter
{
    protected PDO $pdo;
    protected QueryBuilder $queryBuilder;
    protected DataPreparator $dataPreparator;
    protected string $table;
    protected int $fetchMode;
    protected string $daoClass;
    protected int $transactionInProgress = 0;

    public function __construct(PDO $pdo, QueryBuilder $queryBuilder, DataPreparator $dataPreparator)
    {
        $this->pdo = $pdo;
        $this->queryBuilder = $queryBuilder;
        $this->dataPreparator = $dataPreparator;
    }

    public function getPdo(): PDO
    {
        return $this->pdo;
    }

    public function setStmt(string $query, array $args = null): PDOStatement
    {
        if ($args === null) {
            $stmt = $this->pdo->query($query);

            if ($stmt === false) {
                if ($this->transactionInProgress === 1) {
                    $this->rollBack();
                }
                throw new InvalidArgumentException('Unexpected PDO::statement value');
            }

            return $this->setStmtFetchMode($stmt);
        }

        $stmt = $this->pdo->prepare($query);

        if ($stmt->execute($args) === false) {
            if ($this->transactionInProgress === 1) {
                $this->rollBack();
            }

            throw new RuntimeException(__CLASS__ . ': PDOStatement::execute exception.');
        }

        return $this->setStmtFetchMode($stmt);
    }


    public function query(string $query): PDOStatement
    {
        return $this->pdo->query($query);
    }

    /**
     * @throws Exception
     */
    public function multiQuery(string $multiQuery): void
    {
        $this->pdo->beginTransaction();

        try {
            $queries = explode(
                ';',
                trim(preg_replace('/\s+/', ' ', $multiQuery))
            );

            foreach ($queries as $query) {
                if (empty($query)) {
                    continue;
                }
                $this->query($query);
            }

            if ($this->pdo->inTransaction()) {
                $this->pdo->commit();
            }
        } catch (Exception $e) {
            $this->pdo->rollBack();
            throw new Exception($e->getMessage());
        }
    }


    public function beginTransaction(): bool
    {
        $this->transactionInProgress = 1;
        return $this->pdo->beginTransaction();
    }

    public function commit(): bool
    {
        $this->transactionInProgress = 0;
        return $this->pdo->commit();
    }

    public function rollBack(): bool
    {
        $this->transactionInProgress = 0;
        return $this->pdo->rollBack();
    }


    public function getLastInsertId(): string
    {
        return $this->pdo->lastInsertId();
    }


    protected function mergeArgs(array $valuesSets): array
    {
        $args = [];

        foreach ($valuesSets as $values) {
            $args += $values;
        }

        return $args;
    }

    protected function setStmtFetchMode(PDOStatement $stmt): ?PDOStatement
    {
        if ($this->fetchMode === PDO::FETCH_ASSOC) {
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            return $stmt;
        }

        if ($this->fetchMode === PDO::FETCH_CLASS) {
            $stmt->setFetchMode(PDO::FETCH_CLASS, $this->daoClass);
            return $stmt;
        }

        $stmt->setFetchMode((PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE), $this->daoClass);

        return $stmt;
    }
}
