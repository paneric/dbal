<?php

declare(strict_types=1);

namespace Paneric\DBAL;

use Paneric\Interfaces\DataObject\DataObjectInterface;

class Manager extends PDOAdapter
{
    public function getQueryBuilder(): QueryBuilder
    {
        return $this->queryBuilder;
    }

    public function setSelectQuery(?string $selectQuery): Manager
    {
        $this->queryBuilder->setSelectQuery($selectQuery);

        return $this;
    }

    public function setTable(string $table): Manager
    {
        $this->table = $table;

        return $this;
    }

    public function setDAOClass(string $daoClass): Manager
    {
        $this->daoClass = $daoClass;

        return $this;
    }

    public function setFetchMode(int $fetchMode): Manager
    {
        $this->fetchMode = $fetchMode;

        return $this;
    }


    public function findOneBy(array $criteria, string $operator = 'AND'): null|DataObjectInterface|array
    {
        $this->queryBuilder
            ->select($this->table)
            ->where($criteria, $operator);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $criteria
        );

        $result = $stmt->fetch();

        if (!$result) {
            return null;
        }

        return $result;
    }

    public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null): array
    {
        $this->queryBuilder->select($this->table);

        if (!empty($criteria)) {
            if (count($criteria) === 1) {
                $field = array_key_first($criteria);
                if (is_array($criteria[$field])) {
                    $this->queryBuilder->whereIn($criteria[$field], $field);
                    $criteria = $criteria[$field];
                } else {
                    $this->queryBuilder->where($criteria);
                }
            } else {
                $this->queryBuilder->where($criteria);
            }
        }

        if (!empty($orderBy)) {
            $this->queryBuilder->orderBy($orderBy);
        }

        if ($limit !== null) {
            $this->queryBuilder->limit();
            $criteria['limit'] = $limit;
        }

        if ($offset !== null) {
            $this->queryBuilder->offset();
            $criteria['offset'] = $offset;
        }

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $criteria
        );

        return $stmt->fetchAll();
    }


    public function findBySame(array $criteriaSame, string $operator = 'OR'): array
    {
        $criteriaSame = $this->dataPreparator->prepareWhereMultipleDataSet($criteriaSame);

        $this->queryBuilder
            ->select($this->table)
            ->whereSame($criteriaSame, $operator);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $this->mergeArgs($criteriaSame)
        );

        return $stmt->fetchAll();
    }

    public function findByLike(array $criteriaLike, array $operators = ['OR']): array
    {
        $criteriaLike = $this->dataPreparator->prepareWhereMultipleDataSet($criteriaLike);

        $this->queryBuilder
            ->select($this->table)
            ->whereLike($criteriaLike, $operators);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $this->mergeArgs($criteriaLike)
        );

        return $stmt->fetchAll();
    }


    public function create(array $fieldsSet): string
    {
        $this->queryBuilder->insert($this->table, $fieldsSet);

        $this->setStmt(
            $this->queryBuilder->getQuery(),
            $fieldsSet
        );

        return $this->pdo->lastInsertId();
    }

    public function createMultiple(array $fieldsSets): int
    {
        $fieldsSets = $this->dataPreparator->prepareInsertDataSets($fieldsSets);

        $this->queryBuilder->insertMultiple($this->table, $fieldsSets);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $this->dataPreparator->chainDataSets($fieldsSets)
        );

        return $stmt->rowCount();
    }


    public function update(array $fields, array $criteria): int
    {
        $this->queryBuilder->update($this->table, $fields)
            ->where($criteria);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $this->mergeArgs([$fields, $criteria])
        );

        return $stmt->rowCount();
    }
    /*
     * ON DUPLICATE KEY UPDATE - ALL FIELDS HAS TO BE MENTIONED !!!
     * If the new row is inserted, the number of affected-rows is 1.
     * If the existing row is updated, the number of affected-rows is 2.
     * If the existing row is updated using its current values, the number of affected-rows is 0.
     */
    public function updateMultiple(array $fieldsSets, string $idKey): int
    {
        $fieldsSets = $this->dataPreparator->prepareInsertDataSets($fieldsSets);

        $this->queryBuilder->updateMultiple($this->table, $fieldsSets, $idKey);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $this->dataPreparator->chainDataSets($fieldsSets)
        );

        return $stmt->rowCount();
    }

    public function updateSame(array $fields, array $values, string $field): int
    {
        $criteriaInSet = $this->dataPreparator->prepareWhereInDataSet($field, $values);

        $this->queryBuilder->updateSame($this->table, $fields, $criteriaInSet, $field);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $this->mergeArgs([$fields, $criteriaInSet])
        );

        return $stmt->rowCount();
    }


    public function delete(array $criteria): int
    {
        $this->queryBuilder->delete($this->table, $criteria);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $criteria
        );

        return $stmt->rowCount();
    }

    public function deleteMultiple(array $values, string $field): int
    {
        $criteriaInSet = $this->dataPreparator->prepareWhereInDataSet($field, $values);

        $this->queryBuilder->deleteMultiple($this->table, $criteriaInSet, $field);

        $stmt = $this->setStmt(
            $this->queryBuilder->getQuery(),
            $criteriaInSet
        );

        return $stmt->rowCount();
    }
}
