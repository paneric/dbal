<?php

declare(strict_types=1);

namespace Paneric\DBAL;

use Paneric\Interfaces\DataObject\DataObjectInterface;
use PDO;

abstract class Repository
{
    protected Manager $manager;

    protected string $table;
    protected string $daoClass;
    protected int $fetchMode;

    protected ?string $selectQuery = null;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }


    public function adaptManager(int $fetchMode = null): void
    {
        $this->manager->setTable($this->table);
        $this->manager->setDAOClass($this->daoClass);

        $this->manager->setSelectQuery($this->selectQuery);

        if ($fetchMode !== null) {
            $this->manager->setFetchMode($fetchMode);
            return;
        }

        $this->manager->setFetchMode($this->fetchMode);
    }

    public function setSelectQuery(string $selectQuery): void
    {
        $this->selectQuery = $selectQuery;

        $this->manager->setSelectQuery($this->selectQuery);
    }

    public function setFetchMode(int $fetchMode): void
    {
        $this->fetchMode = $fetchMode;

        $this->manager->setFetchMode($fetchMode);
    }

    public function getTable(): string
    {
        return $this->table;
    }


    public function findOneBy(array $criteria): null|DataObjectInterface|array
    {
        $this->adaptManager();

        $queryResult = $this->manager->findOneBy($criteria);

        if ($queryResult === false) {
            return null;
        }

        return $queryResult;
    }

    public function findAll(): array
    {
        $this->adaptManager();

        return $this->manager->findBy([]);
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        $this->adaptManager();

        return $this->manager->findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findBySame(array $criteriaSame, string $operator = 'OR'): array
    {
        $this->adaptManager();

        return $this->manager->findBySame($criteriaSame, $operator);
    }

    public function findByLike(array $criteriaLike, array $operators = ['OR']): array
    {
        $this->adaptManager();

        return $this->manager->findByLike($criteriaLike, $operators);
    }

    public function findByIds(array $criteria): array//['prefix_id' => $ids]
    {
        $this->adaptManager();

        return $this->manager->findBySame($criteria);
    }

    public function findByEnhanced(array $criteria, string $operator = 'AND', $id = null): array
    {
        $this->adaptManager();

        if ($id === null) {
            $queryBuilder = $this->manager->getQueryBuilder();
            $queryBuilder->select($this->table)
                ->where($criteria, $operator);

            $stmt = $this->manager->setStmt(
                $queryBuilder->getQuery(),
                $criteria
            );
        }

        if ($id !== null) {

            $idKey = array_search($id, $criteria, true);

            $queryBuilder = $this->manager->getQueryBuilder();
            $queryBuilder->select($this->table)
                ->where($criteria, $operator)
                ->whereIn([$idKey . '0' => $id], $idKey, 'NOT', 'AND');

            $stmt = $this->manager->setStmt(
                $queryBuilder->getQuery(),
                array_merge($criteria, [$idKey . '0' => $id])
            );
        }

        return $stmt->fetchAll();
    }


    public function getRowsNumber(array $criteria = null): int
    {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->init('select count(*) from ' . $this->table);

        if ($criteria !== null) {
            $queryBuilder->where($criteria);
        }

        $stmt = $this->manager->getPdo()->prepare($queryBuilder->getQuery());
        $stmt->execute($criteria);

        return $stmt->fetchColumn();
    }
}
