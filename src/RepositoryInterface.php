<?php

declare(strict_types=1);

namespace Paneric\DBAL;

use Paneric\Interfaces\DataObject\DataObjectInterface;

interface RepositoryInterface
{
    public function adaptManager(): void;

    public function getTable(): string;

    public function findOneBy(array $criteria): ?DataObjectInterface;

    public function findAll(): array;
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;
    public function findBySame(array $criteriaSame, string $operator = 'OR'): array;
    public function findByLike(array $criteriaLike, array $operators = ['OR']): array;

    public function findByIds(array $criteria): array; //['prefix_id' => $ids]
    public function findByEnhanced(array $criteria, string $operator = 'AND', $id = null): array;

    public function getRowsNumber(array $criteria = null): int;
}
