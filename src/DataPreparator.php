<?php

declare(strict_types=1);

namespace Paneric\DBAL;

class DataPreparator
{
    public function prepareInsertDataSets(array $fieldsSets): array
    {
        $fieldsSetsPrepared[0] = $fieldsSets[0];

        $setsNumber = count($fieldsSets);
        for ($i = 1; $i < $setsNumber; $i++) {
            foreach ($fieldsSets[$i] as $key => $value) {
                $fieldsSetsPrepared[$i][$key . $i] = $value;
            }
        }

        return $fieldsSetsPrepared;
    }

    public function prepareWhereInDataSet(string $field, $values): array
    {
        $values = array_values($values);

        $whereInSet = [$field => $values[0]];

        $setsNumber = count($values);

        for ($i = 1; $i < $setsNumber; $i++) {
            $whereInSet[$field . $i] = $values[$i];
        }

        return $whereInSet;
    }

    public function prepareWhereMultipleDataSet(array $criteria): array
    {
        $whereInSet = [];

        foreach ($criteria as $keyWhere => $values) {

            $whereInSet[$keyWhere][$keyWhere] = $values[0];

            $setsNumber = count($values);

            for ($i = 1; $i < $setsNumber; $i++) {
                $whereInSet[$keyWhere][$keyWhere . $i] = $values[$i];
            }
        }

        return $whereInSet;
    }

    public function chainDataSets(array $fieldsSets): array
    {
        $fieldsSetsChained = [];

        foreach ($fieldsSets as $fields) {
            foreach ($fields as $key => $value) {
                $fieldsSetsChained[$key] = $value;
            }
        }

        return $fieldsSetsChained;
    }
}
