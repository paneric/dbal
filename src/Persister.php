<?php

declare(strict_types=1);

namespace Paneric\DBAL;

use Paneric\Interfaces\DataObject\DataObjectInterface;

class Persister
{
    protected Manager $manager;

    protected string $table;
    protected string $daoClass;
    protected int $fetchMode;

    protected ?string $selectQuery = null;

    protected string $createUniqueWhere;
    protected string $updateUniqueWhere;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }


    public function adaptManager(int $fetchMode = null): void
    {
        $this->manager->setTable($this->table);
        $this->manager->setDAOClass($this->daoClass);

        $this->manager->setSelectQuery($this->selectQuery);

        if ($fetchMode !== null) {
            $this->manager->setFetchMode($fetchMode);
            return;
        }

        $this->manager->setFetchMode($this->fetchMode);
    }


    public function create(DataObjectInterface $dataObject): string
    {
        $this->manager->setTable($this->table);
        return $this->manager->create($dataObject->prepare());
    }

    public function createUnique(array $criteria, DataObjectInterface $dataObject): ?string
    {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->select($this->table);

        $query = $queryBuilder->getQuery() . $this->createUniqueWhere;

        $stmt = $this->manager->setStmt($query, $criteria);

        if (!empty($stmt->fetchAll())) {
            return null;
        }

        $this->manager->setTable($this->table);

        return $this->create($dataObject);
    }

    public function createUniqueMultiple(array $criteria, array $dataObjects): array
    {
        $lastInsertedIds = [];

        foreach ($dataObjects as $index => $dataObject) {
            $lastInsertId = $this->createUnique($criteria[$index], $dataObject);
            if($lastInsertId !== null) {
                $lastInsertedIds[] = (int) $lastInsertId;
                unset($dataObjects[$index]);
            }
        }

        return [
            'data_objects' => $dataObjects,
            'last_inserted_ids' => $lastInsertedIds,
        ];
    }

    public function update(array $criteria, DataObjectInterface $hydrator): int//['prefix_id' => $id]
    {
        $this->manager->setTable($this->table);
        return $this->manager->update($hydrator->prepare(), $criteria);
    }

    public function updateUnique(
        array $criteriaSelect,
        array $criteriaUpdate,
        DataObjectInterface $dataObject
    ): ?int {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->select($this->table);

        $query = $queryBuilder->getQuery() . $this->updateUniqueWhere;

        $stmt = $this->manager->setStmt($query, $criteriaSelect);

        if (!empty($stmt->fetchAll())) {
            return null;
        }

        $this->manager->setTable($this->table);

        return $this->manager->update($dataObject->prepare(true), $criteriaUpdate);
    }

    public function updateUniqueMultiple(
        array $criteriaSelect,
        array $criteriaUpdate,
        array $dataObjects
    ): array {
        foreach ($dataObjects as $index => $dataObject) {
            if($this->updateUnique($criteriaSelect[$index], $criteriaUpdate[$index], $dataObject) !== null) {
                unset($dataObjects[$index]);
            }
        }

        return $dataObjects;
    }

    public function delete(array $criteria): int
    {
        $this->adaptManager();

        $this->manager->setTable($this->table);
        return $this->manager->delete($criteria);
    }

    public function deleteMultiple(array $criteriaDelete, array $collection): array
    {
        foreach ($criteriaDelete as $index => $criteria) {
            if($this->delete($criteria) === 1) {
                unset($collection[$index]);
            }
        }

        return $collection;
    }
}
