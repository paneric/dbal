<?php

declare(strict_types=1);

namespace Paneric\DBAL;

class SequencePreparator
{
    public function prepareInsertSequences(array $fields): array
    {
        $names = '';
        $values = '';

        foreach($fields as $key => $value) {
            $names .= $key.', ';
            $values .= ':'.$key.', ';
        }

        return [
            'names' => '('.rtrim($names, ', ').')',
            'values' => '('.rtrim($values, ', ').')',
        ];
    }

    public function prepareInsertMultipleSequences(array $fieldsSets): array
    {
        $names = '';
        $values = '';
        $valuesChain = '';

        $keys = array_keys($fieldsSets[0]);

        foreach ($keys as $key) {
            $names .= $key.', ';
        }

        foreach ($fieldsSets as $fields) {
            foreach($fields as $key => $value) {
                $values .= ':'.$key.', ';
            }

            $values = '('.rtrim($values, ', ').')';
            $valuesChain .= $values . ', ';
            $values = '';
        }

        return [
            'names' => '('.rtrim($names, ', ').')',
            'values' => rtrim($valuesChain, ', '),
        ];
    }

    public function prepareUpdateSequence(array $fields): string
    {
        $updateSequence = '';

        foreach ($fields as $key => $value) {
            $updateSequence .= $key.'=:'.$key.', ';
        }

        return rtrim($updateSequence, ', ');
    }

    public function prepareOnDuplicateSequences(array $fieldsSet, string $idKey): string
    {
        $onDuplicateSequence = 'ON DUPLICATE KEY UPDATE ';

        unset($fieldsSet[$idKey]);

        $keys = array_keys($fieldsSet);

        foreach($keys as $key) {
            $onDuplicateSequence .= $key.'=VALUES('. $key .'), ';
        }

        return rtrim($onDuplicateSequence, ', ');
    }
}
