<?php

declare(strict_types=1);

namespace Paneric\DBAL;

use Paneric\Interfaces\DataObject\DataObjectInterface;

interface QueryInterface
{
    public function queryAll(): array;
    public function queryBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array;

    public function queryOneBy(array $criteria): ?DataObjectInterface;
}
