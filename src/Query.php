<?php

declare(strict_types=1);

namespace Paneric\DBAL;

use Paneric\Interfaces\DataObject\DataObjectInterface;
use PDO;

abstract class Query
{
    protected Manager $manager;

    protected string $adaoClass;

    protected string $baseQuery;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    public function adaptManager(): void
    {
        $this->manager->setDAOClass($this->adaoClass);
        $this->manager->setFetchMode(PDO::FETCH_CLASS);
    }

    public function queryAll(): array
    {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->init($this->baseQuery);

        $stmt = $this->manager->setStmt($queryBuilder->getQuery());

        return $stmt->fetchAll();
    }

    public function queryBy(array $criteria, array $orderBy = null, $limit = null, $offset = null): array
    {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->init($this->baseQuery);

        if (!empty($criteria)) {
            $queryBuilder->where($criteria);
        }

        if (!empty($orderBy)) {
            $queryBuilder->orderBy($orderBy);
        }

        if ($limit !== null) {
            $queryBuilder->limit();
            $criteria['limit'] = $limit;
        }

        if ($offset !== null) {
            $queryBuilder->offset();
            $criteria['offset'] = $offset;
        }

        $stmt = $this->manager->setStmt(
            $queryBuilder->getQuery(),
            $criteria
        );

        return $stmt->fetchAll();
    }

    public function queryOneBy(array $criteria): ?DataObjectInterface
    {
        $this->adaptManager();

        $queryBuilder = $this->manager->getQueryBuilder();
        $queryBuilder->init($this->baseQuery);

        if (!empty($criteria)) {
            $queryBuilder->where($criteria);
        }

        $stmt = $this->manager->setStmt(
            $queryBuilder->getQuery(),
            $criteria
        );

        $adao = $stmt->fetch();

        if(!$adao) {
            return null;
        }

        return $adao;
    }
}
