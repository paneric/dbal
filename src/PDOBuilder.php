<?php

declare(strict_types=1);

namespace Paneric\DBAL;

use PDO;

class PDOBuilder
{
    public function build(array $config): PDO
    {
        $dsn = sprintf(
            '%s%s%s',
            "mysql:host=".$config['host'],
            ";dbname=".$config['dbName'],
            ";charset=".$config['charset']
        );

        return new PDO(
            $dsn,
            $config['user'],
            $config['password'],
            $config['options']
        );
    }
}
