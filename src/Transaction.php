<?php

declare(strict_types=1);

namespace Paneric\DBAL;

abstract class Transaction
{
    public function __construct(protected Manager $manager)
    {
    }

    public function adaptManager(string $table, string $daoClass, int $fetchMode): void
    {
        $this->manager->setTable($table);
        $this->manager->setDAOClass($daoClass);
        $this->manager->setFetchMode($fetchMode);
    }
}
