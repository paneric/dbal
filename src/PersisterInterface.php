<?php

declare(strict_types=1);

namespace Paneric\DBAL;

use Paneric\Interfaces\DataObject\DataObjectInterface;

interface PersisterInterface
{
    public function adaptManager(): void;

    public function create(DataObjectInterface $dataObject): string;
    public function createUnique(array $criteria, DataObjectInterface $dataObject): ?string;
    public function createUniqueMultiple(array $criteria, array $dataObjects): array;

    public function update(array $criteria, DataObjectInterface $hydrator): int; //['prefix_id' => $id]
    public function updateUnique(array $criteriaSelect, array $criteriaUpdate, DataObjectInterface $dataObject): ?int;
    public function updateUniqueMultiple(array $criteriaSelect, array $criteriaUpdate, array $dataObjects): array;

    public function delete(array $criteria): int;
    public function deleteMultiple(array $criteriaDelete, array $dataObjects): array;
}
