<?php

declare(strict_types=1);

use Paneric\DBAL\Manager;
use Paneric\DBAL\PDOBuilder;
use Paneric\DBAL\QueryBuilder;
use Paneric\DBAL\UserRepository;

require 'vendor/autoload.php';

$config = require_once ('pdo-config.php');

$pdoBuilder = new PDOBuilder();

$manager = new Manager(
    $pdoBuilder->build($config),
    new QueryBuilder()
);
$manager->setTable('fields');

$userRepository = new UserRepository($manager);

$userRepository->findAllByLike(['id' => [1, 2, 3], 'ref' => [1, 2, 3]], ['AND', 'OR', 'AND']);



//$manager->createMultiple([
//    ['ref' => 'ref', 'name' => 'name', 'route' => 'route'],
//    ['ref' => 'ref1', 'name' => 'name1', 'route' => 'route1'],
//    ['ref' => 'ref2', 'name' => 'name2', 'route' => 'route2'],
//]);

//$manager->create(['ref' => 'ref', 'name' => 'name', 'route' => 'route']);


//$manager->updateMultiple([
//    ['id' => 1, 'ref' => 'ref', 'name' => 'name', 'route' => 'route'],
//    ['id' => 2, 'ref' => 'ref1', 'name' => 'name1', 'route' => 'route1'],
//    ['id' => 3, 'ref' => 'ref2', 'name' => 'name2', 'route' => 'route2'],
//]);

//$manager->deleteMultiple([1, 2, 3]);

//$userRepository = new UserRepository($manager);

//var_dump($manager->new(['ref' => 'user9', 'age' => 29]));
//var_dump($userRepository->findAll());

//$queryBuilder = new QueryBuilder();

//$queryBuilder->select('user');
//$queryBuilder->whereLike([
//    'id' => ['id1' => 1, 'id2' => 2],
//    'ref' => ['ref1' => 'user1', 'ref2' => 'user2']
//], [
//    'AND', 'OR', 'AND'
//]);

//$queryBuilder->select('user');
//$queryBuilder->whereLike([
//    'id' => ['id1' => 1, 'id2' => 2]
//], [
//    'OR'
//]);



var_dump($queryBuilder->getQuery());